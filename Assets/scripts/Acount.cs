﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Acount : MonoBehaviour {
	public GameObject accountPanel;
	public Text nickName;
	public Text followers;
	public Text likes;
	public Image photo;
	public Image baseImage;
	public MainGame mainGame;
	int addCoinAfterSeeAds = 50;
	string textNikName;
	public Button freeCoins;
	// Use this for initialization

	public void MyImage(Image myPhoto){
		photo.sprite = myPhoto.sprite;
	}

	void Start () {
		textNikName = PlayerPrefs.GetString ("NickName");
		if (textNikName.Length == 0 || textNikName == null) {
			nickName.text = "Nickname123";
		} else {
			nickName.text = textNikName;
		}
	}
	
	public void RefreshNickName(string value){
		nickName.text = value.ToString ();
		if (nickName.text.Length == 0) {
			nickName.text = "Nickname123";
		} else {
			nickName.text = textNikName;
		}
	}

	public void onAppLovinEventReceived(string textEvent) {
		if (string.Equals(textEvent, "HIDDENINTER"))
		{
		}
		if (string.Equals(textEvent, "LOADFAILED"))
		{
//			AppLovin.PreloadInterstitial();
		}
	}

	public void ActiveAcount(){
		likes.text = PlayerPrefs.GetInt ("totalLikes").ToString ();
		followers.text = PlayerPrefs.GetInt ("totalFollowers").ToString ();
		photo.sprite = baseImage.sprite;


		accountPanel.SetActive (true);

	}

	public void ShowAD(){
		if(AppLovin.HasPreloadedInterstitial()){
			AppLovin.ShowInterstitial();
			mainGame.AddCoins (addCoinAfterSeeAds);
			mainGame.AddCoinsBrBr(addCoinAfterSeeAds);
		}
		else{
			AppLovin.PreloadInterstitial();
		}
	}
	public void OnBack(){
		this.gameObject.SetActive (false);
	}

	public void OnApplicationQuit(){
		if(nickName.text != null){	
			PlayerPrefs.SetString ("NickName",nickName.text);
		}
	}
}

