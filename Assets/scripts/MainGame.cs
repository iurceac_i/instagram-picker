﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using System.Linq;

public class MainGame : MonoBehaviour {
	public GameObject[] eff;
	public Button getCoins;
	public Button uPgrade;
	public Button achievement;
	public Button account;
	public Button settings;
	public Button buyCoins;
	public GameObject upgradePanel;
	public GameObject achievementPanel;
	public GameObject accountPanel;
	public GameObject settingsPanel;
	public GameObject buyCoinsPanel;
	public Text likeCounter;
	public Text followCounter;
	public Text coinsCounter;
	UpgradeController upgradeFile;
	public Slider speedSlider;
	string[] productList;
	List<Background> backgroundLayer;
	public Sprite[] ImageBackground;
	Adapter adaptersuca;
	int totalCoinsinGame;
	int timerValue;
	int brbr;
	string speedLabel;
	public Image basePhoto;
	public Sprite defoultImage;
	public Text speedCount;
	int countLike;
	int countFollow;
	int countCoins;
	int maxSpeedSlide;
	int accelerationSlide;
	bool activeTimer = false;
	Acount acount;
	float currentSpeed;
	float maxSpeed = 1f;
	float acceleration = 0.0025f;
	float timer = 0f;
	string namePhoto;
	public Sprite redButton;
	public Sprite greenButton;
	// Use this for initialization
	float timeRemaining = 3600f;
	int coun;
	float timerSelBG = 0.5f;
	string nameBG;

	void Start () {
		
//		PlayerPrefs.DeleteAll ();
//		PlayerPrefs.SetInt ("totalCoins",9450);

		AppLovin.SetSdkKey("lF1FL5NLs3IrsqOTUqmMvnMLvLqvfJ5OZ7dvC0F34XvwMHTs6EC__ZKuw9oZTuy1_axCbAFfi8a3Q2wXG7WGgs");
		AppLovin.InitializeSdk ();
		AppLovin.PreloadInterstitial();
		AppLovin.HasPreloadedInterstitial ();
		AppLovin.SetUnityAdListener (gameObject.name);
		coun = PlayerPrefs.GetInt ("selectBGPosition");
		getCoins.GetComponent <Button>();
		upgradePanel.SetActive (false);
		settingsPanel.SetActive (false);
		buyCoinsPanel.SetActive (false);
		accountPanel.SetActive (false);
		Application.targetFrameRate = 60;
		speedLabel = PlayerPrefs.GetString ("SpeedLabel");
		accelerationSlide = PlayerPrefs.GetInt ("accelerationSlider");
		maxSpeedSlide = PlayerPrefs.GetInt ("maxSpeedSlider");
		updateSpeedPoints (maxSpeedSlide);
		updateAccelerationPoints (accelerationSlide);
		countCoins = PlayerPrefs.GetInt ("totalCoins");
		countLike = PlayerPrefs.GetInt ("totalLikes");
		countFollow = PlayerPrefs.GetInt ("totalFollowers");
		brbr = PlayerPrefs.GetInt ("brbr");
		followCounter.text = countFollow.ToString ();
		likeCounter.text = countLike.ToString ();
		coinsCounter.text = countCoins.ToString ();

		timerValue = (int)speedSlider.maxValue;
		speedLabel = (timerValue + (maxSpeedSlide * 10)).ToString ();
		speedCount.text = "10";
		if(basePhoto.sprite == null){
			basePhoto.sprite = defoultImage;
		}
	}
	void Update () {
		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch (0);
			timerSelBG -= Time.deltaTime;
				
			if (touch.phase == TouchPhase.Began) {
				timerSelBG -= Time.deltaTime;
				if (timerSelBG <= 0f) {
					coun = PlayerPrefs.GetInt ("selectBGPosition");
					timerSelBG = 0.5f;
				}


				if (coun == 0) {
					eff [0].SetActive (true);	
					eff [0].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [0].SetActive (false);	
					eff [0].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 1) {
					eff [1].SetActive (true);	
					eff [1].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [1].SetActive (false);	
					eff [1].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 2) {
					eff [2].SetActive (true);	
					eff [2].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [2].SetActive (false);	
					eff [2].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 3) {
					eff [3].SetActive (true);	
					eff [3].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [3].SetActive (false);	
					eff [3].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 4) {
					eff [4].SetActive (true);	
					eff [4].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [4].SetActive (false);	
					eff [4].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 5) {
					eff [5].SetActive (true);	
					eff [5].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [5].SetActive (false);	
					eff [5].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 6) {
					eff [6].SetActive (true);	
					eff [6].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [6].SetActive (false);	
					eff [6].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 7) {
					eff [7].SetActive (true);	
					eff [7].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [7].SetActive (false);	
					eff [7].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 8) {
					eff [8].SetActive (true);	
					eff [8].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [8].SetActive (false);	
					eff [8].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 9) {
					eff [9].SetActive (true);	
					eff [9].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [9].SetActive (false);	
					eff [9].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 10) {
					eff [10].SetActive (true);	
					eff [10].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [10].SetActive (false);	
					eff [10].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 11) {
					eff [11].SetActive (true);	
					eff [11].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [11].SetActive (false);	
					eff [1].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 12) {
					eff [12].SetActive (true);	
					eff [12].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [12].SetActive (false);	
					eff [12].GetComponent<ParticleSystem> ().Stop ();
				}
			}

			if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended) { 
				if (coun == 0) {
					eff [0].SetActive (false);	
					eff [0].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [0].SetActive (false);	
					eff [0].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 1) {
					eff [1].SetActive (false);	
					eff [1].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [1].SetActive (false);	
					eff [1].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 2) {
					eff [2].SetActive (false);	
					eff [2].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [2].SetActive (false);	
					eff [2].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 3) {
					eff [3].SetActive (false);	
					eff [3].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [3].SetActive (false);	
					eff [3].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 4) {
					eff [4].SetActive (false);	
					eff [4].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [4].SetActive (false);	
					eff [4].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 5) {
					eff [5].SetActive (false);	
					eff [5].GetComponent<ParticleSystem> ().Play ();
				} else {
					eff [5].SetActive (false);	
					eff [5].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 6) {
					eff [6].SetActive (false);	
					eff [6].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [6].SetActive (false);	
					eff [6].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 7) {
					eff [7].SetActive (false);	
					eff [7].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [7].SetActive (false);	
					eff [7].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 8) {
					eff [8].SetActive (false);	
					eff [8].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [8].SetActive (false);	
					eff [8].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 9) {
					eff [9].SetActive (false);	
					eff [9].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [9].SetActive (false);	
					eff [9].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 10) {
					eff [10].SetActive (false);	
					eff [10].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [10].SetActive (false);	
					eff [10].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 11) {
					eff [11].SetActive (false);	
					eff [11].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [11].SetActive (false);	
					eff [11].GetComponent<ParticleSystem> ().Stop ();
				}
				if (coun == 12) {
					eff [12].SetActive (false);	
					eff [12].GetComponent<ParticleSystem> ().Stop ();
				} else {
					eff [12].SetActive (false);	
					eff [12].GetComponent<ParticleSystem> ().Stop ();
				}
		
			}

		} 
		


// adaugare coinuri la fiecare 1 ora
		timeRemaining -= Time.deltaTime;
		if (timeRemaining > 0)
		{
		}
		if(timeRemaining < 0){
			countCoins = PlayerPrefs.GetInt ("totalCoins");
			countLike = PlayerPrefs.GetInt ("totalLikes");
			countFollow = PlayerPrefs.GetInt ("totalFollowers");
			if (countLike > 0) {
//				countCoins += 1;
				AddCoins ((int)countLike);
				coinsCounter.text = countCoins.ToString ();
			} if(countFollow > 0){
//				countCoins += 2;
				AddCoins ((int)(countFollow *2));
				coinsCounter.text = countCoins.ToString ();
			}

			Debug.Log (countCoins);
			timeRemaining = 3600f;
		}
	
	 
}
	public void UnlockBackgroundItem(int position){
		backgroundLayer [position].set_isLocked (false);
	}
	public List<Background> GetBackgroundImage (){
		if (backgroundLayer == null){
			backgroundLayer = new List<Background> ();
			backgroundLayer.Add (new Background(ImageBackground [0],false));
			backgroundLayer.Add (new Background(ImageBackground [1],false));
			backgroundLayer.Add (new Background(ImageBackground [2],false));
			backgroundLayer.Add (new Background(ImageBackground [3],false));
			backgroundLayer.Add (new Background(ImageBackground [4],false));
			backgroundLayer.Add (new Background(ImageBackground [5],false));
			backgroundLayer.Add (new Background(ImageBackground [6],false));
			backgroundLayer.Add (new Background(ImageBackground [7],false));
			backgroundLayer.Add (new Background(ImageBackground [8],false));
			backgroundLayer.Add (new Background(ImageBackground [9],false));
			backgroundLayer.Add (new Background(ImageBackground [10],false));
		}
		return backgroundLayer;
	}




	public int getTotalCoins(){
		return countCoins;
	}

	public void updateSpeedPoints(int speedPoints){
		maxSpeed = 1 + (Mathf.Pow (speedPoints,3)) / 35f;

//		upgradeFile.AfisareSpeeed ((int)maxSpeed);
	}

	public void updateAccelerationPoints (int accelerationPoints){
		acceleration = 0.0025f + (Mathf.Pow (accelerationPoints, 3)) / 500;


//		upgradeFile.AfisareAcceleratie ((int)acceleration);
	}

	void FixedUpdate () {
		if(activeTimer == true){
			getCoins.image.sprite = redButton;
			getCoins.interactable = false;
			timer += Time.deltaTime;
			if (currentSpeed < maxSpeed) {
				currentSpeed += acceleration;
			} else {
				currentSpeed = maxSpeed;
			}
			speedCount.text = ((int)(currentSpeed * 10)).ToString ();
			speedSlider.value = speedSlider.value + Time.fixedDeltaTime * currentSpeed;
			if (speedSlider.value >= speedSlider.maxValue) {
				getCoins.interactable = true;
				activeTimer = false;
				getCoins.image.sprite = greenButton;
				currentSpeed = 0;
				timer = 0;
			}
		}
	}

	public void OnAddCoins(){
		activeTimer = true;
		speedSlider.value = 0;
		if(getCoins.interactable == true){
			AddCoinsBrBr (4);
			AddCoins (4);
			PlayerPrefs.SetInt ("brbr",brbr);
		}
		coinsCounter.text = countCoins.ToString ();

	}
	public void RefreshSpeed(int valueSpeed){
		maxSpeedSlide = valueSpeed;
		speedLabel = (timerValue + (maxSpeedSlide * 10)).ToString ();
		speedCount.text = speedLabel;
	}

	public void RefreshAcceleration(int valueAcceleration){
		accelerationSlide = valueAcceleration;
	}

//	public void RefreshCounter(int value){
//		coinsCounter.text = value.ToString ();
//	}

	public void RefreshCounterFollowers(int value){
		followCounter.text = value.ToString ();
	}

	public void RefreshCounterLikes(int value){
		likeCounter.text = value.ToString ();
	}

	public void OnUpgrade(){
		upgradePanel.SetActive (true);
		achievementPanel.SetActive (false);
		settingsPanel.SetActive (false);
		buyCoinsPanel.SetActive (false);
	}

	public void OnSettings(){
		upgradePanel.SetActive (false);
		settingsPanel.SetActive (true);
		buyCoinsPanel.SetActive (false);

	}
	public void OnMarketCoins(){
//		marketCons.ReadJson ();
		upgradePanel.SetActive (false);
		settingsPanel.SetActive (false);
		buyCoinsPanel.SetActive (true);
	}

	public void OnBack(){
		upgradePanel.SetActive (false);
		settingsPanel.SetActive (false);
		buyCoinsPanel.SetActive (false);
	}

	void OnApplicationFocus(bool hasFocus)
	{
		if (hasFocus == false) {
			PlayerPrefs.GetString ("SpeedLabel",speedLabel);
			PlayerPrefs.SetInt ("totalCoins", countCoins);
		}
	}
	public void AddCoins(int value){
		countCoins += value;
		coinsCounter.text = countCoins.ToString ();
		PlayerPrefs.SetInt ("totalCoins",countCoins);
	}

	public void AddCoinsBrBr(int value){
		brbr+= value;
		PlayerPrefs.SetInt ("brbr",brbr);
	}

	public void RemoveCoins(int value){
		countCoins -= value;
		coinsCounter.text = countCoins.ToString ();
		PlayerPrefs.SetInt ("totalCoins",countCoins);
	}

	#if UNITY_EDITOR
	void OnApplicationQuit()
	{
			
			PlayerPrefs.GetString ("SpeedLabel",speedLabel);
			PlayerPrefs.SetInt ("totalCoins", countCoins);
	}
	#endif

	public static string GetAppSandboxPathIPhone() {
		// We need to strip "/myappname.app/Data"
		return Path.GetDirectoryName(Path.GetDirectoryName(Application.dataPath));
	}

	public static string GetAppPersistencePathIPhone() {
		return Path.Combine(GetAppSandboxPathIPhone(), "Documents");
	}






}
