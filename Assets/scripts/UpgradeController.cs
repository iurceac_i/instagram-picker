﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeController : MonoBehaviour {
	public Slider maxSpeed;
	public Slider acceleration;
	public GameObject alertPanel;
	int totalCoins;
	int totalFollowers;
	int totalLike;
	public MainGame mainGame;
	int priceUpMaxSpeed = 50;
	int priceUpAcceleration = 20;
	int priceFollowers = 30;
	int priceLike = 12;
	int counterFollowsForBuy = 0;
	int counterLikeForBuy = 0;
	public Button addFollow;
	public Button removeFollow;
	public Button addLike;
	public Button removeLike;
	public Text labelPriceFollowers;
	public Text labelPriceLikes;
	public Text counterFollows;
	public Text counterLike;
	int totalPriceFollow;
	int totalPriceLike;
	public InputField texFil;
	public Text afisareViteza;
	public Text afisareAcceleratie;
	float getViteza;
	float getAcceleratia;
	int playerIntSpeed;
	int playerIntAccel;
	public Text priceSpeedAfis;
	public Text priceAccelAfis;
	public GameObject goFoll;
	public GameObject goLike;

void Start () {
		
	goFoll.SetActive (false);
	goLike.SetActive (false);
	afisareViteza.text = PlayerPrefs.GetFloat ("afisMaxSpeed").ToString ("0.0");
	afisareAcceleratie.text = PlayerPrefs.GetInt ("afisAccel").ToString ();
		if(PlayerPrefs.HasKey ("priceUpSpeed")){
			priceUpMaxSpeed = PlayerPrefs.GetInt ("priceUpSpeed");
			priceSpeedAfis.text = priceUpMaxSpeed.ToString ();
		}
		if(PlayerPrefs.HasKey ("priceUpAccel")){
			priceUpAcceleration = PlayerPrefs.GetInt ("priceUpAccel");
			priceAccelAfis.text = priceUpAcceleration.ToString ();
		}
	counterFollows.text = (0).ToString ();
	counterLike.text = "0";
	alertPanel.SetActive (false);
	maxSpeed.value = PlayerPrefs.GetInt ("maxSpeedSlider");
	acceleration.value = PlayerPrefs.GetInt ("accelerationSlider");
}

public void AddMaxSpeed(){
	if(maxSpeed.value < maxSpeed.maxValue){
		totalCoins = mainGame.getTotalCoins ();
		if (totalCoins >= priceUpMaxSpeed) {
			priceSpeedAfis.text = (priceUpMaxSpeed*2).ToString ();
			maxSpeed.value += 1;
			mainGame.RemoveCoins (priceUpMaxSpeed);
			priceUpMaxSpeed *= 2;
			priceSpeedAfis.text = priceUpMaxSpeed.ToString ();
			PlayerPrefs.SetInt ("priceUpSpeed", priceUpMaxSpeed);
			PlayerPrefs.SetInt ("maxSpeedSlider",(int)maxSpeed.value);
			getViteza = 1.0f + (Mathf.Pow ((int)maxSpeed.value,3)) / 35;
			PlayerPrefs.SetFloat ("afisMaxSpeed", getViteza);
			mainGame.updateSpeedPoints ((int)maxSpeed.value);
			afisareViteza.text = (getViteza).ToString ("0.0");
		} else {
			alertPanel.SetActive (true);
		}
	}
}

public void AddAcceleration(){
	if(acceleration.value < acceleration.maxValue){
		totalCoins = mainGame.getTotalCoins ();
		if(totalCoins >= priceUpAcceleration){
			acceleration.value += 1;
			mainGame.RemoveCoins (priceUpAcceleration);
			priceUpAcceleration *= 2;
			priceAccelAfis.text = priceUpAcceleration.ToString ();
			PlayerPrefs.SetInt ("priceUpAccel", priceUpAcceleration);
			PlayerPrefs.SetInt ("accelerationSlider",(int)acceleration.value);
			getAcceleratia = 0.0025f + (Mathf.Pow (acceleration.value, 3)) / 500;
			playerIntAccel = ((int)(getAcceleratia * 100));
			mainGame.updateAccelerationPoints ((int)acceleration.value);
			PlayerPrefs.SetInt ("afisAccel", playerIntAccel+5);
			afisareAcceleratie.text = (playerIntAccel+5).ToString();
		} else {
			alertPanel.SetActive (true);
		}
	}
}
	public void OnCloseAlertPanel(){
		alertPanel.SetActive (false);
	}

	void OnApplicationQuit(){
		PlayerPrefs.SetInt ("maxSpeedSlider",(int)maxSpeed.value);
		PlayerPrefs.SetInt ("accelerationSlider",(int)acceleration.value);
	}

	void OnApplicationFocus(bool hasFocus){
		if (hasFocus == false) {
			PlayerPrefs.SetInt ("maxSpeedSlider",(int)maxSpeed.value);
			PlayerPrefs.SetInt ("accelerationSlider",(int)acceleration.value);
		}
	}

	public void onAddFollowers(){
		totalCoins = PlayerPrefs.GetInt ("totalCoins");
		if(totalCoins >= ((counterFollowsForBuy + 1) * priceFollowers)){
			counterFollowsForBuy++;
			counterFollows.text = counterFollowsForBuy.ToString ();
			totalPriceFollow = counterFollowsForBuy * priceFollowers;
			labelPriceFollowers.text = totalPriceFollow.ToString (); 

		}else {
			alertPanel.SetActive (true);
		}
	}

	public void onRemoveFollowers(){
		if(counterFollowsForBuy > 0){
			counterFollowsForBuy--;

		}
		counterFollows.text = counterFollowsForBuy.ToString ();
		labelPriceFollowers.text = (counterFollowsForBuy * priceFollowers).ToString ();
	}

	public void onAddLike(){
		totalCoins = PlayerPrefs.GetInt ("totalCoins");
		if (totalCoins >= ((counterLikeForBuy+1) * priceLike)) {
			counterLikeForBuy++;
			totalPriceLike = counterLikeForBuy * priceLike;
			labelPriceLikes.text = totalPriceLike.ToString (); 
			counterLike.text = counterLikeForBuy.ToString ();
		} else {
			alertPanel.SetActive (true);
		}
	}

	public void onRemoveLike(){
		if(counterLikeForBuy > 0){
			counterLikeForBuy--;
		}
		labelPriceLikes.text = (counterLikeForBuy * priceLike).ToString ();
		counterLike.text = counterLikeForBuy.ToString ();
	}

	public void OnBack(){
		RessetCounters ();
		this.gameObject.SetActive (false);
	}

	public void BuyFollowers(){
		totalCoins = mainGame.getTotalCoins ();
		totalFollowers = PlayerPrefs.GetInt ("totalFollowers");
		if(totalCoins >= totalPriceFollow){
			mainGame.RemoveCoins (totalPriceFollow);
			totalFollowers = totalFollowers + counterFollowsForBuy;
			PlayerPrefs.SetInt ("totalFollowers",totalFollowers);
			Debug.Log ("totalFollowers " + totalFollowers);
			mainGame.RefreshCounterFollowers (totalFollowers);
			RessetCounters ();
		} else{
			alertPanel.SetActive (true);
		}
	}

	public void BuyLikes(){
		totalCoins = mainGame.getTotalCoins ();
		totalLike = PlayerPrefs.GetInt ("totalLikes");
		if(totalCoins >= totalPriceLike){
			mainGame.RemoveCoins (totalPriceLike);
			totalLike = totalLike + counterLikeForBuy;
			PlayerPrefs.SetInt ("totalLikes",totalLike);
			mainGame.RefreshCounterLikes (totalLike);
			RessetCounters ();
		}else{
			alertPanel.SetActive (true);
		}
	}

	public void RessetCounters(){
		counterFollowsForBuy = 0;
		counterLikeForBuy = 0;
		totalPriceFollow = 0;
		totalPriceLike = 0;
		counterLike.text = counterLikeForBuy.ToString ();
		counterFollows.text = counterFollowsForBuy.ToString ();
		labelPriceLikes.text = totalPriceLike.ToString ();
		labelPriceFollowers.text = totalPriceFollow.ToString ();
	}

	public void ActiveEveyFollPanel(){
		goFoll.SetActive (true);

	}
	public void ActiveEveryLikePanel(){
		goLike.SetActive (true);
	}
	public void OnCloseFollPan(){
		goFoll.SetActive (false);
	}
	public void OnCloseLike(){
		goLike.SetActive (false);
	}

}
