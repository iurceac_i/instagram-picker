﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class startBG : MonoBehaviour {
	public MainGame maingame;
	public Image gameObjectSprite;
	// Use this for initialization
	void Start () {
		List<Background> backgroundSprite = maingame.GetBackgroundImage ();
		Background b = backgroundSprite [PlayerPrefs.GetInt ("selectBGPosition")];
		changeBackground (b);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void changeBackground(Background listParamSprite){
		gameObjectSprite.GetComponent<Image> ().sprite = listParamSprite.get_image();
	}



}
