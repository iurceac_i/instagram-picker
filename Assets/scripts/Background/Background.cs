﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background{
	private	Sprite image;
	private bool isLocked;
	public Background (Sprite img, bool isLocked)
	{
		this.isLocked = isLocked;
		image = img;
	}
	public Sprite get_image ()
	{
		return image;
	}
	public bool get_isLocked ()
	{
		return isLocked;
	}
	public void set_isLocked(bool param){
		isLocked = param;
	}
}
