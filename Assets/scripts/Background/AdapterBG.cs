﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class AdapterBG : MonoBehaviour {
	public GameObject gameObjectBG;
	MainGame mainG;
	private OnItemClickListenerBG adapterCallBack;
	int oldSelected  = -1;
	// Use this for initialization
	public void SetAdapter(List<Background> background){
		for (int i = 0; i < background.Count; i++) {
			Background s = background [i];
			GameObject imageGameObject = gameObjectBG.transform.GetChild (0).gameObject;
			Image imageBackground = imageGameObject.GetComponent<Image> ();

			imageBackground.sprite = s.get_image ();
			GameObject finalObj = Instantiate (gameObjectBG, transform);
			finalObj.SetActive (true);
			finalObj.name = i.ToString ();
			Button bt = finalObj.transform.GetChild (2).gameObject.GetComponent<Button> ();
			bt.onClick.AddListener (() => OnItemClick (finalObj));
		}
	}

	public interface OnItemClickListenerBG {
		void onItemClicked(int position);
	}

	public void registerListener(OnItemClickListenerBG callback){
		this.adapterCallBack = callback;
	}

	private void OnItemClick(GameObject go){
		int position = Int32.Parse (go.name);
//		mainG.GetSelectedBG ((int)position);
//		PlayerPrefs.SetInt ("bgPosition",position);
		if (adapterCallBack != null) {
			adapterCallBack.onItemClicked (position);
		}
	}

	public void SetSelectedBG(int position){
		if (oldSelected == -1) {
			setItemSelected (position, true);	
		} else {
			if(position != oldSelected){
				setItemSelected (position, true);
				setItemSelected (oldSelected, false);
			}
		}
		oldSelected = position;

	}

	private void setItemSelected(int posSelect, bool selected){
		GameObject curentGO = transform.GetChild (posSelect).gameObject;
		GameObject btnSelected = curentGO.transform.GetChild (1).gameObject;
		btnSelected.SetActive (selected);
	}
}

