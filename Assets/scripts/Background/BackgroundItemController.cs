﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundItemController : MonoBehaviour, AdapterBG.OnItemClickListenerBG {
	public AdapterBG adapterBG;
	public Image backgroundImg;
	public MainGame mainGame;
	public GameObject lisstBackgroundPanel;
	public int numCount = 0;
	// Use this for initialization
	void Start () {
		adapterBG.SetAdapter (mainGame.GetBackgroundImage());
		int position = PlayerPrefs.GetInt ("selectBGPosition");
		adapterBG.SetSelectedBG (position);
		adapterBG.registerListener (this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region OnItemClickListenerBG implementation
	public void onItemClicked (int position)
	{
		numCount++;
		if (numCount == 3) {
			AppLovin.ShowInterstitial ();
			numCount = 0;
		}
			PlayerPrefs.SetInt ("selectBGPosition", position);
			adapterBG.SetSelectedBG (position);
			List<Background> backgroundList;
			backgroundList = mainGame.GetBackgroundImage();
			Background b = backgroundList [position]; 
			backgroundImg.sprite = b.get_image();
	}

	#endregion
}
