﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Achievements : MonoBehaviour {
	public Toggle totalCoinsToggle;
	public Toggle totalFollowersToggle;
	public Toggle totalLikesToggle;
	public GameObject totalCoinsTabPanel;
	public GameObject totalFollowersTabPanel;
	public GameObject totalLikesTabPanel;
	public GameObject achievementPanel;
	public Text totalLikes;
	public Text totalFollowers;
	public Text totalCoins;
	public Image imgForBg;
	public Image achiveImg;
//	public GameObject scorePanel;


	// Use this for initialization
	void Start () {
		totalLikesToggle.isOn = true;

	}
	

	public void OnAchievements(){
		achievementPanel.SetActive (true);
		achiveImg.sprite = imgForBg.sprite;
		totalLikes.text = PlayerPrefs.GetInt ("totalLikes").ToString ();
		totalFollowers.text = PlayerPrefs.GetInt ("totalFollowers").ToString ();
		totalCoins.text = PlayerPrefs.GetInt ("brbr").ToString ();
		totalLikesToggle.isOn = true;
	}

	public void ToggleLike(){
		if(totalLikesToggle.isOn){
			totalCoinsTabPanel.SetActive (false);
			totalFollowersTabPanel.SetActive (false);
			totalLikesTabPanel.SetActive (true);

		}
	}

	public void ToggleFollower(){
		if(totalFollowersToggle.isOn){
			totalCoinsTabPanel.SetActive (false);
			totalFollowersTabPanel.SetActive (true);
			totalLikesTabPanel.SetActive (false);

		}
	}

	public void ToggleCoins(){
		if(totalCoinsToggle.isOn){
			totalCoinsTabPanel.SetActive (true);
			totalFollowersTabPanel.SetActive (false);
			totalLikesTabPanel.SetActive (false);

		}
	}
	public void OnBack(){
		this.gameObject.SetActive (false);
	}
}
