﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Settings : MonoBehaviour {
	public GameObject backgroundList;
	Acount acount;
	public GameObject nickNamePanel;
	public Text getNickName;
	string savetNickname;
	public Text labelNikname;
	public MainGame mainGm;

	void Start () {
		backgroundList.SetActive (false);
	}

	public void FeedBack(){
		#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1269024260?action=write-review");
		#endif
	}
	public void RateUseButton(){
		#if UNITY_ANDROID
		Application.OpenURL("market://details?id=YOUR_APP_ID");
		#elif UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=1269024260");
		#endif
	
	}
	public void OnActiveBackgroundList(){
		backgroundList.SetActive (true);
	}
	public void OnCLosePanelSettings ()	{

		this.gameObject.SetActive (false);

	}

	public void OnCloseBGList(){
		backgroundList.SetActive (false);
	}
	public void CloseNickNamePanel(){
		nickNamePanel.SetActive (false);
	}
	public void ChangeNickName(){
		nickNamePanel.SetActive (true);
	}
	public void Text_Changed (string nickname){
		savetNickname = nickname;
	}

	public void SaveNickName(){
		labelNikname.text = savetNickname;
		PlayerPrefs.SetString ("NickName",savetNickname);

		nickNamePanel.SetActive (false);

		Debug.Log (savetNickname);
	}


}
