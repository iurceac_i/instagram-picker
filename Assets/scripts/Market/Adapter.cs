﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Adapter : MonoBehaviour {
	public GameObject gameObjectCoin;
	private OnItemClickListenerPurchase adapterCallBack;
	public void SetElement(Item[] Item){
	
		for (int i = 0; i < Item.Length; i++) {
			Item item = Item [i];
			GameObject  titleButtonBuy = gameObjectCoin.transform.GetChild (2).GetChild (0).gameObject;
			Text priceInButton = titleButtonBuy.GetComponent<Text> ();
			priceInButton.text = item.get_priceButton ();

			GameObject  namePurchase = gameObjectCoin.transform.GetChild (3).gameObject;
			Text nameIAP = namePurchase.GetComponent<Text> ();
			nameIAP.text = item.get_name ();	

		

			GameObject finalObj = Instantiate (gameObjectCoin, transform);
			finalObj.SetActive (true);
			finalObj.name = i.ToString ();

			GameObject buttonBuy = finalObj.transform.GetChild (2).gameObject;
			Button buyCoins = buttonBuy.GetComponent<Button> ();
			buyCoins.onClick.AddListener (() => OnItemClick (finalObj));
	
		}
	}

	public void OnItemClick(GameObject go){
		int position = Int32.Parse (go.name);
		if (adapterCallBack != null) {
			adapterCallBack.onItemClicked (position);

		}
	}
	public interface OnItemClickListenerPurchase {
		void onItemClicked(int position);
	}

	public void registerListener(OnItemClickListenerPurchase callback){
		this.adapterCallBack = callback;
	}

}
