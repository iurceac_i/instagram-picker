﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class Item {
public int count;
public string name;
public string titleButton;
public string productId;

	public int getCount(){
		return count;
	}

	public string get_name(){
		return name;
	}

	public string get_priceButton(){
		return titleButton;
	}

	public string get_product_Id(){
		return productId;
	}

}
