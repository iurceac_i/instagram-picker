﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Purchasing;
using System;

public class InAppManager : MonoBehaviour,Adapter.OnItemClickListenerPurchase, IStoreListener {
	public Adapter adapter;
	public MainGame mainGame;
	public Text nrCoins;
	Main itemsContent;
	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;

	string productID;
	// Use this for initialization
	void Start () {
		ReadJson ();

		if (m_StoreController == null)
		{
			InitializePurchasing();
		}
	}


	public void ReadJson(){
		string path = "/Raw/purchases.json";
		#if UNITY_EDITOR
		path = "/StreamingAssets/purchases.json";
		#endif

		string json = File.ReadAllText(Application.dataPath + path);
		itemsContent = JsonUtility.FromJson<Main> (json); 
		adapter.SetElement (itemsContent.content);
		adapter.registerListener (this);
	}

	#region OnItemClickListenerPurchase implementation

	public void onItemClicked (int position)
	{
		Item[] allItems = itemsContent.get_content ();
		productID = allItems [position].get_product_Id ();
		BuyProductID (productID);
	}

	#endregion

	/// <summary>
	/// In-App-Purchase IOS
	/// </summary>



	#region IStoreListener implementation
	public void InitializePurchasing()
	{
		if (IsInitialized())
		{
			return;
		}

		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		Item[] allItems = itemsContent.get_content ();
		for(int i = 0; i <allItems.Length; i++ ){
			builder.AddProduct(allItems[i].get_product_Id (),ProductType.Consumable);
		}

		UnityPurchasing.Initialize(this, builder);
	}

	private bool IsInitialized()
	{
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void BuyProductID(string productId)
	{
		try
		{
			if (IsInitialized())
			{
				Product product = m_StoreController.products.WithID(productId);

				if (product != null && product.availableToPurchase)
				{
					Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
					m_StoreController.InitiatePurchase(product);
				}
				else
				{
					Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			}
			else
			{
				Debug.Log("BuyProductID FAIL. Not initialized.");
			}
		}
		catch (Exception e)
		{
			Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
		}
	}

	public void RestorePurchases()
	{
		if (!IsInitialized())
		{
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
		{
			Debug.Log("RestorePurchases started ...");

			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			apple.RestoreTransactions((result) =>
				{
					Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				});
		}
		else
		{
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log("OnInitialized: Completed!");

		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

		string requestPurchaseID = args.purchasedProduct.definition.id;
		int coins = GetProductCoins (requestPurchaseID);
		if( coins!= 0){
			mainGame.AddCoins (coins);
			mainGame.AddCoinsBrBr(coins);
		}
		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}
	#endregion


	public int GetProductCoins(string id){
		Item[] allItems = itemsContent.get_content ();
		for (int i = 0; i < allItems.Length; i++){
			string product = allItems [i].get_product_Id ();
			if (product == id) {
				return allItems[i].getCount (); 
			}
		}
		return 0;
	}
}